from construct import Int8sl, Int16sl, Int16ul, Int32sl, Int32ul, Int64sl, Int64ul

u16 = Int16ul
u32 = Int32ul
u64 = Int64ul

i8 = Int8sl
i16 = Int16sl
i32 = Int32sl
i64 = Int64sl
