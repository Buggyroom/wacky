# v1.0.2
- Fix bool properties being incorrectly jsonified to True all the time
- Fix bool property being read incorrectly when used in a Property
- Fix typo that resulted in bug when jsonifying an ArrayProperty
- Fix UArray value_type being read from the incorrect level in the hierarchy

# v1.0.1
- Force utf-8 encoding for the yaml/json file used in upack+repack cli commands

# v1.0.0
- Removed the loader based on pyUE4Parse

# v0.1.0
- Initial support for loading / dumping + unpacking / repacking