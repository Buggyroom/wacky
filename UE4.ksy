meta:
  id: ue4
  file-extension: uasset
  endian: le
  bit-endian: le
seq:
  - id: magic
    contents: [0xc1, 0x83, 0x2A, 0x9E]
  - id: legacy_file_version
    type: s4
  - id: version_ue3
    type: s4
    if: legacy_file_version != -4
  - id: file_version_ue4
    type: s4
  - id: file_version_licensee_ue4
    type: s4
  - id: num_custom_versions
    type: u4
    if: legacy_file_version <= -2
  - id: custom_versions
    type: custom_version
    repeat: expr
    repeat-expr: num_custom_versions
    if: legacy_file_version <= -2
  - id: total_header_size
    type: u4
  - id: folder_name
    type: string
  - id: package_flags
    type: package_flags
  - id: num_names
    type: u4
  - id: ofs_names
    type: u4
  - id: localization_id
    type: string
    if: file_version_ue4 >= 516 # VER_UE4_ADDED_PACKAGE_SUMMARY_LOCALIZATION_ID
  - id: num_gatherable_text_data
    type: u4
    if: file_version_ue4 >= 459 or file_version_ue4 == 0 # VER_UE4_SERIALIZE_TEXT_IN_PACKAGES
  - id: ofs_gatherable_text_data
    type: u4
    if: file_version_ue4 >= 459 or file_version_ue4 == 0 # VER_UE4_SERIALIZE_TEXT_IN_PACKAGES
  - id: num_exports
    type: u4
  - id: ofs_exports
    type: u4
  - id: num_imports
    type: u4
  - id: ofs_imports
    type: u4
  - id: ofs_depends
    type: u4
  - id: num_soft_package_references
    type: u4
    if: file_version_ue4 >= 384 or file_version_ue4 == 0 # VER_UE4_ADD_STRING_ASSET_REFERENCES_MAP
  - id: ofs_soft_package_references
    type: u4
    if: file_version_ue4 >= 384 or file_version_ue4 == 0 # VER_UE4_ADD_STRING_ASSET_REFERENCES_MAP
  - id: ofs_searchable_names
    type: u4
    if: file_version_ue4 >= 510 or file_version_ue4 == 0 # VER_UE4_ADDED_SEARCHABLE_NAMES
  - id: ofs_thumbnail_table
    type: u4
  - id: guid
    type: guid
  - id: num_generations
    type: u4
  - id: generations
    type: generation
    repeat: expr
    repeat-expr: num_generations
  - id: saved_by_engine_version
    type: engine_version
    if: file_version_ue4 >= 336 or file_version_ue4 == 0
  - id: compatible_with_engine_version
    type: engine_version
    if: file_version_ue4 >= 444 or file_version_ue4 == 0
  - id: compression_flags
    type: compression_flags
  - id: num_compressed_chunks
    type: u4
  - id: compressed_chunks
    type: compressed_chunk
    repeat: expr
    repeat-expr: num_compressed_chunks
  - id: package_source
    type: u4
    doc: Value that is used to determine if the package was saved by Epic (or licensee) or by a modder, etc
  - id: num_additional_packages_to_cook
    type: u4
  - id: additional_packages_to_cook
    type: string
    repeat: expr
    repeat-expr: num_additional_packages_to_cook
  - id: num_texture_allocations
    contents: [0,0,0,0]
    if: legacy_file_version > -7
  - id: ofs_asset_registry
    type: u4
  - id: ofs_bulk_data
    type: u8
  - id: ofs_world_tile_info
    type: u4
  - id: num_chunk_ids
    type: u4
  - id: chunk_ids
    type: u4
    repeat: expr
    repeat-expr: num_chunk_ids
  - id: num_preload_dependency
    type: u4
  - id: ofs_preload_dependency
    type: u4
  - id: names
    type: name
    repeat: expr
    repeat-expr: num_names
  - id: imports_map
    type: import_map_entry
    repeat: expr
    repeat-expr: num_imports
  - id: exports_map
    type: export_map_entry
    repeat: expr
    repeat-expr: num_exports
  - id: depends_map
    type: depends_map_entry
    repeat: expr
    repeat-expr: num_exports
  - id: num_asset_objects
    type: u4
  - id: asset_objects
    type: u4 # todo : implement
    repeat: expr
    repeat-expr: num_asset_objects
  - id: preload_dependencies
    type: preload_dependency(exports_map[_index])
    repeat: expr
    repeat-expr: num_exports
  - id: exports
    type: export(exports_map[_index])
    repeat: expr
    repeat-expr: num_exports
  - id: magic_2
    contents: [0xc1, 0x83, 0x2A, 0x9E]
types:
  preload_dependency:
    params:
      - id: map_entry
        type: export_map_entry
    seq:
      - id: serialization_before_serialization_dependencies
        type: package_index
        repeat: expr
        repeat-expr: map_entry.num_serialization_before_serialization_dependencies
      - id: create_before_serialization_dependencies
        type: package_index
        repeat: expr
        repeat-expr: map_entry.num_create_before_serialization_dependencies
      - id: serialization_before_create_dependencies
        type: package_index
        repeat: expr
        repeat-expr: map_entry.num_serialization_before_create_dependencies
      - id: create_before_create_dependencies
        type: package_index
        repeat: expr
        repeat-expr: map_entry.num_create_before_create_dependencies
  depends_map_entry:
    seq:
      - id: num_items
        type: u4
      - id: items
        type: package_index
        repeat: expr
        repeat-expr: num_items
  custom_version_array:
    seq:
      - id: num_versions
        type: u4
      - id: versions
        type: custom_version
        repeat: expr
        repeat-expr: num_versions
  custom_version:
    seq:
      - id: key
        type: guid
      - id: version
        type: s4
  guid:
    seq:
      - id: parts
        type: u4
        repeat: expr
        repeat-expr: 4
  string:
    seq:
      - id: signed_len
        type: s4
      - id: bytes
        size: len_bytes
    instances:
      encoding:
        value: signed_len < 0 ? "UTF-16LE" : "UTF-8"
      len_text:
        value: 'signed_len < 0 ? -signed_len : signed_len'
      len_bytes:
        value: 'signed_len < 0 ? 2 * len_text : len_text'
      raw_text:
        value: bytes.to_s(encoding)
      text:
        value: raw_text.substring(0, raw_text.length - 1)
  package_flags:
    seq:
      - id: newly_created
        type: b1
        doc: Newly created package, not saved yet. In editor only
      - id: client_optional
        type: b1
        doc: Purely optional for clients
      - id: server_side_only
        type: b1
        doc: Only needed on the server side
      - type: b1
      - id: compiled_in
        type: b1
        doc: 'This package is from "compiled in" classes'
      - id: for_diffing
        type: b1
        doc: This package was loaded just for the purposes of diffing
      - id: editor_only
        type: b1
        doc: This is editor-only package (for example: editor module script package)
      - id: developer
        type: b1
        doc: Developer module
      - id: uncooked_only
        type: b1
        doc: Loaded only in uncooked builds (i.e. runtime in editor)
      - id: cooked
        type: b1
        doc: Package is cooked
      - id: contains_no_asset
        type: b1
        doc: Package doesn't contain any asset object (although asset tags can be present)
      - type: b1
      - type: b1
      - id: unversioned_properties
        type: b1
        doc: Uses unversioned property serialization instead of versioned tagged property serialization
      - id: contains_map_data
        type: b1
        doc: Contains map data (UObjects only referenced by a single ULevel) but is stored in a different package
      - type: b1
      - id: compiling
        type: b1
        doc: Package is currently being compiled
      - id: contains_map
        type: b1
        doc: package contains a ULevel/UWorld object
      - id: requires_localization_gather
        type: b1
        doc: package contains some data to be gathered by localization
      - type: b1
      - id: play_in_editor
        type: b1
        doc: package was created for the purpose of PIE
      - id: contains_script
        type: b1
        doc: Package is allowed to contain UClass objects
      - id: disallow_export
        type: b1
        doc: Editor should not export asset in this package
      - type: b1
      - type: b1
      - type: b1
      - type: b1
      - type: b1
      - id: dynamic_imports
        type: b1
        doc: This package should resolve dynamic imports from its export at runtime
      - id: runtime_generated
        type: b1
        doc: This package contains elements that are runtime generated, and may not follow standard loading order rules
      - id: reloading_for_cooker
        type: b1
        doc: This package is reloading in the cooker, try to avoid getting data we will never need. We won't save this package.
      - id: filter_editor_only
        type: b1
        doc: Package has editor-only data filtered out
  generation:
    seq:
      - id: num_exports
        type: u4
      - id: num_names
        type: u4
  engine_version:
    seq:
      - id: major
        type: u2
      - id: minor
        type: u2
      - id: patch
        type: u2
      - id: changelist
        type: u4
      - id: branch
        type: string
  compression_flags:
    seq:
      - id: zlib
        type: b1
        doc: Compress with ZLIB - DEPRECATED, USE FNAME
      - id: gzip
        type: b1
        doc: Compress with GZIP - DEPRECATED, USE FNAME
      - id: custom
        type: b1
        doc: Compress with user defined callbacks - DEPRECATED, USE FNAME
      - id: reserved1
        type: b1
      - id: bias_memory
        type: b1
        doc: Prefer smaller files
      - id: bias_speed
        type: b1
        doc: Prefer faster compression
      - id: reserved2
        type: b1
      - id: source_is_padded
        type: b1
        doc: Is the source buffer padded out ?
      - id: reserved3
        type: b24
  compressed_chunk:
    seq:
      - id: len_compressed
        type: u4
      - id: ofs_compressed
        type: u4
      - id: len_uncompressed
        type: u4
      - id: ofs_uncompressed
        type: u4
  name:
    seq:
      - id: name
        type: string
      - if: _root.file_version_ue4 < 504
        type: u4
  import_map_entry:
    seq:
      - id: class_package
        type: name_reference
      - id: class_name
        type: name_reference
      - id: outer_index
        type: package_index
      - id: object_name
        type: name_reference
  name_reference:
    seq:
      - id: index
        type: u4
      - id: number
        type: u4
    instances:
      referenced_name:
        value: _root.names[index].name.text
      is_none:
        value: referenced_name == "None"
  export_map_entry:
    seq:
      - id: class_index
        type: package_index
      - id: super_index
        type: package_index
      - id: template_index
        type: package_index
      - id: outer_index
        type: package_index
      - id: object_name
        type: name_reference
      - id: object_flags
        type: object_flags
      - id: len_serial
        type: u8
      - id: ofs_serial
        type: u8
      - id: forced_export
        type: bytes_as_bool(4)
      - id: not_for_client
        type: bytes_as_bool(4)
      - id: not_for_server
        type: bytes_as_bool(4)
      - id: package_guid
        type: guid
      - id: package_flags
        type: u4
      - id: not_always_loaded_for_editor_game
        type: bytes_as_bool(4)
      - id: is_asset
        type: bytes_as_bool(4)
      - id: first_export_dependency
        type: s4
      - id: num_serialization_before_serialization_dependencies
        type: u4
      - id: num_create_before_serialization_dependencies
        type: u4
      - id: num_serialization_before_create_dependencies
        type: u4
      - id: num_create_before_create_dependencies
        type: u4
  object_flags:
    seq:
      - id: public
        type: b1
      - id: standalone
        type: b1
      - id: mark_as_native
        type: b1
      - id: transactional
        type: b1
      - id: class_default_object
        type: b1
      - id: archetype_object
        type: b1
      - id: transient
        type: b1
      - id: mark_as_root_set
        type: b1
      - id: tag_garbage_temp
        type: b1
      - id: need_initialization
        type: b1
      - id: need_load
        type: b1
      - id: keep_for_cooker
        type: b1
      - id: need_post_load
        type: b1
      - id: need_post_load_subobjects
        type: b1
      - id: newer_version_exists
        type: b1
      - id: begin_destroyed
        type: b1
      - id: finish_destroyed
        type: b1
      - id: begin_regenerated
        type: b1
      - id: default_subobject
        type: b1
      - id: was_loaded
        type: b1
      - id: text_export_transient
        type: b1
      - id: load_completed
        type: b1
      - id: inheritable_component_template
        type: b1
      - id: duplicate_transient
        type: b1
      - id: strong_ref_on_frame
        type: b1
      - id: non_pie_duplicate_transient
        type: b1
      - id: dynamic
        type: b1
      - id: will_be_loaded
        type: b1
      - id: has_external_storage
        type: b1
      - type: b3
  bytes_as_bool:
    params:
      - id: size
        type: u1
    seq:
      - id: as_bytes
        type: u1
        repeat: expr
        repeat-expr: size
    instances:
      as_bool:
        value: as_bytes.max != 0
  package_index:
    seq:
      - id: index
        type: s4
    instances:
      kind:
        value: >
          index == 0
          ? kind::null
          : (
            index > 0
            ?
              kind::export
            :
              kind::import
          )
      as_import:
        value: -index - 1
      as_export:
        value: index - 1
    enums:
      kind:
        1: "null"
        2: "import"
        3: "export"
  dependency_list:
    seq:
      - id: num_dependencies
        type: u4
      - id: dependencies
        type: package_index
        repeat: expr
        repeat-expr: num_dependencies
  export:
    params:
      - id: map_entry
        type: export_map_entry
    seq:
      - id: inline_export_type
        type: name_reference
        if: "map_entry.class_index.kind == package_index::kind::null"
      - id: value
        type:
          switch-on: export_type.referenced_name
          cases:
            '"DataTable"': data_table
    instances:
      export_type:
        value: >
          map_entry.class_index.kind == package_index::kind::null
          ?
            inline_export_type
          :
            (
              map_entry.class_index.kind == package_index::kind::import
              ?
                _root.imports_map[map_entry.class_index.as_import].object_name
              :
                (
                  map_entry.super_index.kind == package_index::kind::export
                  ?
                    _root.exports_map[map_entry.super_index.as_export].object_name
                  :
                    _root.imports_map[map_entry.super_index.as_import].object_name
                )
            )
  data_table:
    seq:
      - id: uobject
        type:
          switch-on: _root.package_flags.unversioned_properties
          cases:
            false: uobject(true, _parent.map_entry.object_flags.class_default_object)
      - id: num_rows
        type: u4
      - id: rows
        type: row
        repeat: expr
        repeat-expr: num_rows
    # instances:
    #   struct_name:
    #     value: uobject.properties[0].tag.type.referenced_name # Oversimplification, you're supposed to look for the index with tag name == "RowStruct"
    types:
      row:
        seq:
          - id: name
            type: name_reference
          - id: uobject
            type: uobject(false, false)
  uobject:
    params:
      - id: read_has_guid_next
        type: bool
      - id: class_default_object
        type: bool
    seq:
      - id: properties
        type: uobject_property
        repeat: until
        repeat-until: _.tag.name.is_none
      - id: has_guid_next
        type: bytes_as_bool(4)
        if: read_has_guid_next
      - id: guid
        type: guid
        if: read_has_guid_next and has_guid_next.as_bool and not class_default_object
  debug_uobject:
    params:
      - id: read_has_guid_next
        type: bool
      - id: class_default_object
        type: bool
      - id: num_properties
        type: u1
    seq:
      - id: properties
        type: uobject_property
        repeat: expr
        repeat-expr: num_properties
      - id: has_guid_next
        type: bytes_as_bool(4)
        if: read_has_guid_next
      - id: guid
        type: guid
        if: read_has_guid_next and has_guid_next.as_bool and not class_default_object
  uobject_property:
    seq:
      - id: tag
        type: property_tag
      - id: value
        type: property_value(tag, tag.type)
  property_value:
    params:
      - id: tag
        type: property_tag
      - id: type
        type: name_reference
    seq:
      - id: value
        type:
          switch-on: type.referenced_name
          cases:
            '"ByteProperty"': byte_or_enum
            '"BoolProperty"': bool_property
            '"IntProperty"': s4
            '"FloatProperty"': f4
            '"ObjectProperty"': package_index
            '"NameProperty"': name_reference
            '"DelegateProperty"': name_reference
            '"DoubleProperty"': f8
            '"ArrayProperty"': array_property
            # '"StructProperty"': struct_property # Unused by the DataTable file
            '"StrProperty"': string
            # '"TextProperty"': # TODO
            # '"LazyObjectProperty"': # TODO
            # '"SoftObjectProperty"': # TODO
            # '"AssetObjectProperty"': # TODO
            '"UInt64Property"': u8
            '"UInt32Property"': u4
            '"UInt16Property"': u2
            '"Int64Property"': s8
            '"Int16Property"': s2
            '"Int8Property"': s1
            # '"MapProperty"': # TODO
            # '"SetProperty"': # TODO
            '"EnumProperty"': enum_property
            '"Guid"': guid
        if: not tag.name.is_none
    types:
      byte_or_enum:
        seq:
          - id: value
            type:
              switch-on: _parent.tag.value.as<name_reference>.is_none
              cases:
                true: u1
                false: enum_property
      enum_property:
        seq:
          - id: value
            type:
              switch-on: _root.package_flags.unversioned_properties
              cases:
                false: name_reference
      bool_property:
        seq:
          - id: value
            type:
              switch-on: _root.package_flags.unversioned_properties
              cases:
                false: bytes_as_bool(1)
      array_property:
        seq:
          - id: num_items
            type: u4
          - id: item_tag
            type: property_tag
            if: 'item_type_name == "StructProperty" or item_type_name == "ArrayProperty"'
          - id: values
            type: property_value(item_tag, item_type)
            repeat: expr
            repeat-expr: num_items
        instances:
          item_type:
            value: _parent.tag.value.as<name_reference>
          item_type_name:
            value: item_type.referenced_name
      # struct_property:
      #    seq:
      #     - id: value
      #       size: 0
      #       type:
      #         switch-on: struct_name
      #         cases:
      #           "": {} # TODO: implement
      #   instances:
      #     struct_name:
      #       value: _parent.tag.value.as<property_tag::struct>.name.referenced_name
  property_tag:
    seq:
      - id: name
        type: name_reference
      - id: type
        type: name_reference
        if: not name.is_none
      - id: size
        type: s4
        if: not name.is_none
      - id: array_index
        type: s4
        if: not name.is_none
      - id: value
        type:
          switch-on: type.referenced_name
          cases:
            '"StructProperty"': struct
            '"BoolProperty"': bytes_as_bool(1)
            '"ByteProperty"': name_reference
            '"EnumProperty"': name_reference
            '"ArrayProperty"': name_reference
            '"SetProperty"': name_reference
            '"MapProperty"': map
        if: not name.is_none and type.number == 0
      - id: has_guid_next
        type: bytes_as_bool(1)
        if: not name.is_none
      - id: reserved
        type: guid
        if: (not name.is_none) and has_guid_next.as_bool
    types:
      struct:
        seq:
          - id: name
            type: name_reference
          - id: guid
            type: guid
      map:
        seq:
          - id: key_type
            type: name_reference
          - id: value_type
            type: name_reference
