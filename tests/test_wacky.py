from contextlib import contextmanager
from importlib import resources
from tempfile import NamedTemporaryFile
from typing import Iterator, Tuple, BinaryIO

from wacky import load, dump, jsonify, update

from . import data


def test_that_loading_then_dumping_gives_back_same_bytes():
    with resources.open_binary(data, "MusicParameterTable.uasset") as uasset:
        with resources.open_binary(data, "MusicParameterTable.uexp") as uexp:
            original_uasset = uasset.read()
            original_uexp = uexp.read()
            uasset.seek(0)
            uexp.seek(0)
            package = load(uasset=uasset, uexp=uexp)

    with NamedTemporaryFile("wb+") as f_uasset:
        with NamedTemporaryFile("wb+") as f_uexp:
            dump(package, f_uasset, f_uexp)
            f_uasset.seek(0)
            f_uexp.seek(0)
            assert original_uasset == f_uasset.read()
            assert original_uexp == f_uexp.read()
            
    # breakpoint()


def test_that_jsonifying_then_updating_does_not_change_anything():
    with resources.open_binary(data, "MusicParameterTable.uasset") as uasset:
        with resources.open_binary(data, "MusicParameterTable.uexp") as uexp:
            original_package = load(uasset, uexp)
            uasset.seek(0)
            uexp.seek(0)
            changes = jsonify(uasset, uexp)
            uasset.seek(0)
            uexp.seek(0)
            new_package = update(uasset, uexp, changes)
            assert original_package == new_package

@contextmanager
def open_example(basename: str) -> Iterator[Tuple[BinaryIO, BinaryIO]]:
    with resources.open_binary(data, f"{basename}.uasset") as uasset:
        with resources.open_binary(data, f"{basename}.uexp") as uexp:
            yield uasset, uexp

def load_example(basename: str) -> None:
    with open_example(basename) as (uasset, uexp):
        load(uasset, uexp)

def test_loading_stage_up_message():
    load_example("StageUpMessage")

def test_loading_stage_up_table():
    load_example("StageUpTable")

def test_loading_sugoroku_message():
    load_example("SugorokuMessage")

def test_loading_sugoroku_unique_parameter_table():
    load_example("SugorokuUniqueParameterTable")

def test_loading_ticket_message():
    load_example("TicketMessage")

def test_loading_user_plate_background_message():
    load_example("UserPlateBackgroundMessage")
